import logoImg from "@/assets/logo.svg";
import Link from "next/link";
import classes from "./main-header.module.css";
import Image from "next/image";
import MainHeaderBackground from "./main-header-background";
import Navlink from "./nav-link";

export default function MainHeader() {
  return (
    <>
      <MainHeaderBackground />
      <header className={classes.mainheader}>
        <Link href="/">
          <Image
            src={logoImg}
            alt="fish logo"
            className={classes.logo}
            priority
          />
          Next Level Food
        </Link>
        <nav className={classes.navbar}>
          <ul className={classes.navlist}>
            <li className={classes.navitem}>
              <Navlink href="/meals">Browse Meals</Navlink>
            </li>
            <li className={classes.navitem}>
              <Navlink href="/community"> Foodies Community</Navlink>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
}
