"use client";
import { useRef, useState } from "react";
import classes from "./image-picker.module.css";
import Image from "next/image";

export default function ImagePicker({ label, name }) {
  const [pickedImage, setPickedImage] = useState();
  const imageInputRef = useRef();
  function handlePickClick() {
    imageInputRef.current.click();
  }

  function handleImageChange(event) {
    // Check if there are files selected
    if (!event.target.files || event.target.files.length === 0) {
      setPickedImage(null);
      return;
    }

    // Access the first selected file
    const file = event.target.files[0];

    // Check if file is valid
    if (!file) {
      setPickedImage(null);
      return;
    }

    // Read the file as a data URL
    const fileReader = new FileReader();
    fileReader.onload = () => {
      // Set the picked image using the result from FileReader
      setPickedImage(fileReader.result);
    };
    fileReader.onerror = (error) => {
      // Handle FileReader errors
      console.error("Error reading the file:", error);
      setPickedImage(null);
    };

    // Start reading the file
    fileReader.readAsDataURL(file);
  }

  return (
    <div className={classes.picker}>
      <label htmlFor="image">{label}</label>
      <div className={classes.controls}>
        <div className={classes.preview}>
          {!pickedImage && <p>No image selected</p>}
          {pickedImage && (
            <Image
              src={pickedImage}
              alt="the image selected by the user"
              fill
            />
          )}
        </div>
        <input
          className={classes.input}
          type="file"
          id={name}
          accept="image/png, image/jpeg"
          name={name}
          ref={imageInputRef}
          onChange={handleImageChange}
          required
        />
        <button
          className={classes.button}
          type="button"
          onClick={handlePickClick}
        >
          Pick an Image
        </button>
      </div>
    </div>
  );
}
