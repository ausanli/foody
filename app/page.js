import Link from "next/link";
import classes from "./page.module.css";
import ImageSlideshow from "@/components/images/image-slideshow";


export default function Home() {
  return (
    <div className={classes.container}>
      <header className={classes.header}></header>
      <div className={classes.slideshow}>
        <ImageSlideshow />
      </div>
      <div className={classes.content}>
        <div className={classes.hero}>
          <h1>NextLevel Food for NextLevel Foodies</h1>
          <p>Taste & share food from all over the world</p>
        </div>
        <div className={classes.cta}>
          <Link href="/community">Join the community</Link>
          <Link href="/meals">Explore Meals</Link>
        </div>
      </div>
      <main>
        <section className={classes.section}>
          <div className={classes.sectionContent}>
            <h2>Discover How It Works</h2>
            <p>
              Welcome to NextLevel Food, the ultimate platform for food
              enthusiasts! We're here to make your culinary journey
              extraordinary. Here's how it all works:
            </p>

            <div className={classes.step}>
              <div className={classes.sectionContent}>
                <h3>Explore Diverse Meals</h3>
                <p>
                  Browse through a vast collection of unique and delicious meals
                  from all around the world. From traditional dishes to modern
                  twists, there's something for everyone.
                </p>
              </div>
            </div>

            <div className={classes.step}>
              <div className={classes.sectionContent}>
                <h3>Share Your Culinary Creations</h3>
                <p>
                  Are you a talented home chef? Share your culinary creations
                  with our community. Post recipes, photos, and cooking tips to
                  inspire others.
                </p>
              </div>
            </div>

            <div className={classes.step}>
              <div className={classes.sectionContent}>
                <h3>Join the Community</h3>
                <p>
                  Connect with like-minded foodies from around the globe.
                  Participate in discussions, exchange ideas, and make new
                  friends who share your passion for food.
                </p>
              </div>
            </div>

            <div className={classes.step}>
              <div className={classes.sectionContent}>
                <h3>Stay Updated</h3>
                <p>
                  Stay updated with the latest food trends, cooking techniques,
                  and food-related news. Our community is always buzzing with
                  exciting culinary discoveries.
                </p>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}
