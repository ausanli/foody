"use server";
export async function shareMeal(formData) {
  const meal = {
    title: formData.get("title"), // get input field value by name property
    summary: formData.get("summary"),
    instructions: formData.get("instructions"),
    image: formData.get("image"),
    creator: formData.get("name"),
    creator_email: formData.get("email"),
  };
  console.log(meal);
}
